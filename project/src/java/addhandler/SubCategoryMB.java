
package addhandler;

import dao.AddDao;
import dao.ListDao;
import entity.Category;
import entity.SubCategory;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

@ManagedBean(name = "obj2") 
@SessionScoped
public class SubCategoryMB 
{
    SubCategory subcat = new SubCategory();
    Category category = new Category();
    String catname;
    List<Category> listcat;

    
    // Setter & Getter
    public List<Category> getListcat() {
        return listcat;
    }

    public void setListcat(List<Category> listcat) {
        this.listcat = listcat;
    }

    public SubCategory getSubcat() {
        return subcat;
    }

    public void setSubcat(SubCategory sbucat) {
        this.subcat = subcat;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
    
    // Setter & Getter close
    
    
    
    
    
    
    
    public String addSubCategory()
    {
        listcat = new ListDao().catListByName(catname);
        
        category.setCatId(listcat.get(0).getCatId());
        
        subcat.setCategory(category);
        subcat.setSubCatName(subcat.getSubCatName());
        subcat.setSubCatDesc(subcat.getSubCatDesc());
        
        boolean status = new AddDao().addSubCategory(subcat);
        if(status)
        { 
            FacesContext.getCurrentInstance().addMessage(null, new 
            FacesMessage(FacesMessage.SEVERITY_INFO, "Data Saved Successfully", ""));
        } 
        else
        { 
            FacesContext.getCurrentInstance().addMessage(null, new 
                         FacesMessage(FacesMessage.SEVERITY_WARN, "Data not Saved", ""));
        } 
        return null;
    }
    
    
    public List<SelectItem> getCategoryName()
    {
        List<SelectItem> catname = new ListDao().catList();
        
        return catname;
    }
}









