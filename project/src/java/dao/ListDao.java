
package dao;

import entity.Category;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import util.HibernateUtil;

public class ListDao 
{
    public List catList()
    {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        
        List<Category> cList = session.createQuery("SELECT al.catName FROM Category al").list();
        cList.toString();
        
        session.close();
        return cList;
    }
    
    
    // CatList by name
    public List<Category> catListByName(String name)
    {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        
        List<Category> cList = session.createQuery
        ("SELECT al FROM Category al where lower(catName) = '"+ name.toLowerCase() +"'").list();
        cList.toString();
        
        session.close();
        return cList;
    } 
} 
